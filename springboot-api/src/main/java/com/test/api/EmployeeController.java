package com.test.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

//run test url =http://localhost:8181/employee
@RestController
public class EmployeeController {
	
	List<EmployeeBean> theEMP = new ArrayList<>(Arrays.asList(
			new EmployeeBean(1, "vipawadee", 20000L),
			new EmployeeBean(2, "nattawat", 50000L)
			));

	@RequestMapping("/employee")
	public List<EmployeeBean> getAllCosmetics() {
		return theEMP;
	}
	
	@RequestMapping(value = "/employee/{id}", method = RequestMethod.GET)
	public EmployeeBean getCosmetics(@PathVariable int id) {
		EmployeeBean tempEMP = new EmployeeBean();
			
		for (int i = 0; i < theEMP.size(); i++) {		 
			if (theEMP.get(i).getId().equals(id)) {		
				tempEMP.setId(theEMP.get(i).getId());
				tempEMP.setName(theEMP.get(i).getName());
				tempEMP.setSalary(theEMP.get(i).getSalary());
			}
		}		   
		return tempEMP;	
	}

	@RequestMapping(value = "/employee", method = RequestMethod.POST)
	public List<EmployeeBean> postCosmetics(@RequestBody EmployeeBean tempEMP ) {	
		tempEMP.setId(theEMP.size()+1);
		tempEMP.setName(tempEMP.getName());
		tempEMP.setSalary(tempEMP.getSalary());	
		theEMP.add(tempEMP);
		return theEMP;
	}
	
	@RequestMapping(value = "/employee/{id}", method = RequestMethod.PUT)
	public String  putCosmetics(@PathVariable int id,@RequestBody EmployeeBean tempEMP) {	
		String restPons = null;
		System.out.println("update put byId :"+id);
		for (int i = 0; i < theEMP.size(); i++) {
			EmployeeBean index = theEMP.get(i);
			if (index.getId().equals(id)) {
				index.setName(tempEMP.getName());
				index.setSalary(tempEMP.getSalary());	
				restPons="update :"+id+" success!";
			}else {
				restPons="not found :"+id;
			}
		}
		return restPons;
	}
	
	
	@RequestMapping(value = "/employee/{id}", method = RequestMethod.DELETE)
	public String  deleteCosmetics(@PathVariable int id) {
		String restPons = null;
		for (int i = 0; i < theEMP.size(); i++) {
			if (theEMP.get(i).getId().equals(id)) {
				theEMP.remove(i);
				restPons="delete :"+id+" success!";
			}else {
				restPons="not found :"+id;
			}
		}
		return restPons;
	}
	



}